from django.apps import AppConfig


class InvoiceGuiConfig(AppConfig):
    name = 'invoice_gui'
