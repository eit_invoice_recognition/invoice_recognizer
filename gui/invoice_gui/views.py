from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

import sys
sys.path.insert(0, r'd:\ELTE\011 projektek\2017-12 OTP\800 src\invoice_recognizer')
from business_objects import Invoice, InvoiceClass, InvoiceRecognitionResponse

import json
import jsonpickle


def index(request):
    #return HttpResponse("Hello, world. You're at the polls index.")
    template = loader.get_template('invoice_gui/szamla_template.html')
    #test_json_path = r'd:\ELTE\011 projektek\2017-12 OTP\800 src\invoice_recognizer\test\invoice_1001-HMDI_000000002.json' # TODO: remove hard-coded path
    #f = open(test_json_path)
    #json_str = f.read()
    json_str = '{"py/object": "business_objects.Invoice", "currency": null, "customer": null, "date_of_invoice": "2017.01.03.","invoice_number": "KE17/02570","issuer": null, "net_price": null, "payment_date": "2017.01.03.", "payment_due": "2017.01.18.","payment_method": "Atutalas","price": 72390.0}'
    obj = jsonpickle.decode(json_str)
    '''
    with open(test_json_path) as json_file:
        json_data = json.load(json_file)
        print(json_data)
        invoice_response = jsonpickle.decode(frozen) #as InvoiceRecognitionResponse
    '''
    context = {
#        'szallito_nev': obj.invoice.issuer.name,
        'invoice_number' : obj.invoice_number,
        'szamla_kelte' : obj.date_of_invoice,
        'fizetes_modja' : obj.payment_method,
        'fizetesi_hatarido' : obj.payment_due,
        'total_gross_amount' : obj.price,
        'due_for_payment' : obj.price
    }
    return HttpResponse(template.render(context, request))
