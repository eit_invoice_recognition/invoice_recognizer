import pickle
import io
from business_objects import Invoice
from typing import Dict

def get_invoice() -> Dict[str, Invoice]:
    with io.open('ui/pickled_invoice', 'rb') as f:
        return pickle.load(f)


if __name__ == "__main__":
    invoice_dictionary = get_invoice()
    invoice_name, invoice = invoice_dictionary.popitem()
    print("Invoice to be displayed is: |{}| - |{}|".format(invoice_name, invoice))
    # TODO:  Add UI code