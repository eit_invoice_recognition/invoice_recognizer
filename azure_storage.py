from azure.storage.file import FileService
from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
from azure.common import AzureConflictHttpError, AzureHttpError
from io import BytesIO
from typing import Dict, Any
import os
import sys
import logging
parent_path = os.path.abspath(os.path.join('..'))
if parent_path not in sys.path:
    sys.path.append(parent_path)
from logger_setup import getLogger


STORAGE_ACCOUNT_NAME = 'vszmstorage'
STORAGE_ACCOUNT_KEY = '3vV76xzJ2W9YBappM/qMiZkrrV4h/9rE/4MNyS73ipDbjfP3bHmvzwwjBKF7bFRRuwUa5sG6Cqj1V6mXrF8cAQ=='
"""

Our big files are not optimal to be stored in git so we use Azure Cloud Storage service. 
https://azure.microsoft.com/en-us/services/storage/

Here is a short manual on how to store and access large files in our project:
1. Download and Install Azure Storage Explorer from: https://azure.microsoft.com/en-us/features/storage-explorer/
2. Add a connection using storage account name and key and provide the ones you see at the top of this file in constants
3. Click refresh all. Now you should be able to see our Storage and upload documents manually
4. Use get_file_stream_from_cloud wherever you need to reach these uploaded files

"""



logger = getLogger(__name__)
logging.getLogger('urllib3.connectionpool').setLevel(logging.WARNING)
logging.getLogger('azure.storage').setLevel(logging.WARNING)
logging.getLogger('azure.cosmosdb.table').setLevel(logging.WARNING)


def __download_callback(current, total):
    logger.info('Azure download progress: %f %%', current/float(total) * 100)


file_service = FileService(account_name=STORAGE_ACCOUNT_NAME, account_key=STORAGE_ACCOUNT_KEY)
table_service = TableService(account_name=STORAGE_ACCOUNT_NAME, account_key=STORAGE_ACCOUNT_KEY)

def create_table(table_name: str) -> None:

    table_service.create_table(table_name)

def get_entity_from_cloud_table(table_name: str, key: str) -> TableService:
    try:
        return table_service.get_entity(table_name, '', key)
    except AzureHttpError:
        logger.exception('Failed to get entity from table |%s| using rowkey |%s| due to:', table_name, key)
        return None

def store_entity_in_table(table_name: str, entity: Dict[str, Any]) -> bool:
    try:
        table_service.insert_entity(table_name, entity)
        return True
    except AzureConflictHttpError as azureException:
        logger.error('Failed to store entity |%s| in table |%s| because of: |%s|', entity, table_name, azureException)
        return False


def get_file_stream_from_cloud(file_name: str) -> BytesIO:
    """
        Gets a file from our Azure Cloud Storage to byte stream.
    """
    logger.info('Getting file |%s| from Azure', file_name)
    output_stream = BytesIO()
    file_service.get_file_to_stream('ebizfiles', None, file_name, output_stream, progress_callback=__download_callback)
    output_stream.seek(0)
    return output_stream