import io
from PIL import Image

def scale_image_to_filesize(img_bytes: bytes, max_size: int) -> bytes:
    scaled_bytes = img_bytes
    current_size = len(PIL_img_to_bytes(Image.open(io.BytesIO(img_bytes))))
    scale = 1.0

    while current_size > max_size:
        scale = scale * 0.9
        scaled_bytes = PIL_img_to_bytes(Image.open(io.BytesIO(img_bytes)), scale)
        current_size = len(scaled_bytes)

    return scaled_bytes


def PIL_img_to_bytes(img: Image, scale_percent: float = 1.0) -> bytes:
    byte_stream = io.BytesIO()
    img_info = img.info
    img = img.resize((int(img.width * scale_percent), int(img.height * scale_percent)))
    
    if img.mode in ('RGBA', 'LA'):
        fill_color = '#ffffff'
        background = Image.new(img.mode[:-1], img.size, fill_color)
        background.paste(img, img.split()[-1])
        background.info = img.info
        img = background

    if 'exif' in img.info:
        img.save(byte_stream, format='JPEG', supsampling=-1, quality=90, optimize=True, exif=img_info['exif'], icc_profile=img_info.get('icc_profile'))
    else:
        img.save(byte_stream, format='JPEG', supsampling=-1, quality=90, optimize=True, icc_profile=img_info.get('icc_profile'))

    return byte_stream.getvalue()