import io
import os
import sys

from typing import List
from business_objects import OCRMethod
from invoiceparser.ocr import get_ocr_engine
from invoiceparser.pdf_util import convert_image_pdf_to_images
from logger_setup import getLogger
from business_objects import Fragment, InvoiceDocument, YDirection
from preprocess.image_enhancer import noise_removal

parent_path = os.path.abspath(os.path.join('..'))
if parent_path not in sys.path:
    sys.path.append(parent_path)


logger = getLogger(__name__)


def __get_image_fragment_list(file_name: str, file_bytes: bytes, page: int, ocr_method: OCRMethod, use_cache: bool, preprocess: bool) -> List[Fragment]:
    if preprocess:
        image_bytes = noise_removal(file_bytes)
    else:
        image_bytes = file_bytes

    engine = get_ocr_engine(ocr_method)

    fragment_list = engine.get_fragment_list(file_name, image_bytes, use_cache=use_cache)

    for fragment in fragment_list:
        fragment.page = page

    return fragment_list

def ocr_process_image_to_fragments(file_name: str, file_bytes: bytes, ocr_method: OCRMethod = OCRMethod.AZURE, use_cache: bool = True, preprocess: bool = False) -> InvoiceDocument:
    """
        Gets the OCR json output format from a pdf or jpeg image file. 
    """
    if file_name.endswith('.pdf'):
        images_bytes = convert_image_pdf_to_images(file_bytes)
        page_number = 1
        fragment_list = []
        splitted = file_name.split('.')
        base_name = ''.join(splitted[:-1])
        for image_bytes in images_bytes:
            logger.info("file: %d, image: %d", len(file_bytes), len(image_bytes))
            numbered_name = f'{base_name}_{page_number}.jpeg'
            fragment_list.extend(__get_image_fragment_list(numbered_name, image_bytes, page_number, ocr_method, use_cache, preprocess))
            page_number = page_number + 1
    else:
        fragment_list = __get_image_fragment_list(file_name, file_bytes, 1, ocr_method, use_cache, preprocess)

    fragments = InvoiceDocument(fragment_list, YDirection.DOWN)


    if len(fragments.fragment_list) == 0:
        logger.warning('Could not get any fragments from image |%s| using %s method!', file_name, ocr_method)
        logger.debug('Image bytes are: |%s|', file_bytes)

    return fragments

def ocr_process_imagefile_to_fragments(image_path: str) -> List[Fragment]:
    logger.info('Converting image |%s| to a list of Fragments', image_path)
    

    with io.open(image_path, 'rb') as f:
        fragments = ocr_process_image_to_fragments(os.path.basename(image_path), f.read())

        
        return fragments

if __name__ == '__main__':
    ocr_process_imagefile_to_fragments('../dataset/3/1001-HMDI_000000255-ebiz_handmade_document_in.pdf1.jpg')
