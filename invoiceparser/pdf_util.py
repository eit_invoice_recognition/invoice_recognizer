import io
import os
import sys
import PyPDF2 
import shutil
from tika import parser
from pdf2image import convert_from_path, convert_from_bytes
from logger_setup import getLogger
from typing import List
from functional import seq
import tempfile
from .image_util import PIL_img_to_bytes
parent_path = os.path.abspath(os.path.join('..'))
if parent_path not in sys.path:
    sys.path.append(parent_path)
from invoiceparser.image_util import PIL_img_to_bytes

logger = getLogger(__name__)

def pdfimages(PDFfilename):
    pages = convert_from_path(PDFfilename, 500)
    coun=1
    for page in pages:
        finname=PDFfilename+str(coun)+'.jpg'
        page.save(finname, 'JPEG')
        coun=coun+1
        copy_rename(finname,'JPEG'+finname,"Images")
        os.remove(finname)
        

def copy_rename(old_file_name, new_file_name,subfold):
        src_dir= os.curdir
        dst_dir= os.path.join(os.curdir , subfold)
        src_file = os.path.join(src_dir, old_file_name)
        shutil.copy(src_file,dst_dir)
        
        dst_file = os.path.join(dst_dir, old_file_name)
        new_dst_file_name = os.path.join(dst_dir, new_file_name)
        os.rename(dst_file, new_dst_file_name)
        


def convert_image_pdf_to_images(pdf_bytes: bytes) -> List[bytes]:
    """converts a bytestream of a PDF document into a list of jpeg bytestream."""
    return seq(convert_from_bytes(pdf_bytes))\
            .map(lambda PIL_img: PIL_img_to_bytes(PIL_img)).to_list()
    

def is_text_pdf_tika(pdf_bytes: bytes) -> bool:
    fd, path = tempfile.mkstemp()
    try:
        with os.fdopen(fd, 'wb') as tmp:
            tmp.write(pdf_bytes)
            tmp.flush()
            raw = parser.from_file(path)
            # For image pdfs there can be 1-2 non-empty lines for title and meta data, but no more in our random experiments. Upping it to 5 just to be sure
            return 'content' in raw and raw['content'] != None and seq(raw['content'].split('\n')).count(lambda line: len(line) > 0) > 5
    finally:
        os.remove(path)



def is_text_pdf_pypdf2(pdf_bytes: bytes) -> bool:
    try:
        pdf_reader = PyPDF2.PdfFileReader(io.BytesIO(pdf_bytes), strict = False)
    except IOError:
        pass

    if pdf_reader.isEncrypted:
        pdf_reader.decrypt('')

    return len(pdf_reader.getPage(0).extractText()) > 0

def is_text_pdf(pdf_bytes: bytes) -> bool:
    return is_text_pdf_tika(pdf_bytes)

if __name__ == "__main__":
    path = r'E:\Ocr project\pythonocr'

    folder = os.fsencode(path)

    filenames = []

    for file in os.listdir(folder):
        filename = os.fsdecode(file)
        if filename.endswith( ('.pdf') ): # find pdf files
            filenames.append(filename)
            pdfFileObj = open(filename,'rb')
            try:
                pdfReader = PyPDF2.PdfFileReader(pdfFileObj,strict = False)
            except IOError:
                pass
            num_pages = pdfReader.numPages
            pages = convert_from_path(filename, 500)
            
            text = ""
            count = 0
            #The while loop will read each page
            while count < num_pages:
                pageObj = pdfReader.getPage(count)
                count +=1
                text += pageObj.extractText()
                
            if text=="":             # in case the PDF contains only images
                pdfimages(filename)
                
                
            else:                   # in case the PDF contains text
                if not os.path.exists('Text'):
                    os.makedirs('Text')
                    
                copy_rename(filename,'txt'+filename,"Text")
                
            
        if (filename.endswith( ('.jpeg') ) | filename.endswith( ('.jpg') )):    # find image files
            if not os.path.exists('Images'):
                    os.makedirs('Images')
            filenames.append(filename) #######
            copy_rename(filename,'img'+filename,"Images")
        print(file)