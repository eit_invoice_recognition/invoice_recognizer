import io

# pip3 install pdfminer.six
# pip3 install chardet
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter, XMLConverter, HTMLConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage

from bs4 import BeautifulSoup
from business_objects import Fragment, InvoiceDocument, YDirection
from typing import List
from io import BytesIO
from logger_setup import getLogger
import hashlib
import logging

logger = getLogger(__name__)
logging.getLogger('pdfminer').setLevel(logging.WARNING)



def __convert_text_pdf(fp, format='xml', codec='utf-8', password=''):
    rsrcmgr = PDFResourceManager()
    retstr = BytesIO()
    laparams = LAParams()
    if format == 'text':
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    elif format == 'html':
        device = HTMLConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    elif format == 'xml':
        device = XMLConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    else:
        raise ValueError('provide format, either text, html or xml!')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    maxpages = 0
    caching = True
    pagenos=set()
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=False):
        interpreter.process_page(page)

    text = retstr.getvalue().decode()
    device.close()
    retstr.close()
    
    return text




def __convert_xml_textline_to_fragment(textline, page) -> Fragment:
    bbox = textline['bbox']
    bbox_split = bbox.split(',')
    x = float(bbox_split[0])
    y = float(bbox_split[1])
    width = float(bbox_split[2]) - x
    height = float(bbox_split[3]) - y
    text = ''.join([text.string for text in textline.findAll("text") if text.string is not None]).strip()
    return Fragment(text, x, y, width, height, page)

def __convert_fp_to_fragments(fp) -> InvoiceDocument:
    xml_tree = BeautifulSoup(__convert_text_pdf(fp), 'xml')
    page_number = 1
    fragment_list = []
    for page in xml_tree.findAll('page'):
        fragment_list = fragment_list + [__convert_xml_textline_to_fragment(textline, page_number) for textline in page.findAll("textline")]
        page_number = page_number + 1
    
    return InvoiceDocument(fragment_list, YDirection.UP)

def convert_pdf_bytes_to_fragments(pdf_bytes: bytes) -> InvoiceDocument:
    return __convert_fp_to_fragments(BytesIO(pdf_bytes))



def convert_pdf_to_fragments(path :str) -> InvoiceDocument:
    with open(path, 'rb') as fp:
        return __convert_fp_to_fragments(fp)
    
