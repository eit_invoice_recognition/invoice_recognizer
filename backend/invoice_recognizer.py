from business_objects import Invoice, InvoiceClass, InvoiceRecognitionRequest, InvoiceRecognitionResponse
from classifier.fragments_classifier import classify_fragments
from invoiceparser.text_pdf_parser import convert_pdf_bytes_to_fragments
from invoiceparser.image_parser import ocr_process_image_to_fragments
from analyzer.fragment_annotation import FragmentAnnotationML, FragmentAnnotationRuleBased
from flask_api import status
from invoiceparser.pdf_util import is_text_pdf
from logger_setup import getLogger

logger = getLogger(__name__)


SUPPORTED_CLASSES = [InvoiceClass.TEXT_PDF, InvoiceClass.MACHINE_PRINTED]

def recognize_invoice(request: InvoiceRecognitionRequest) -> InvoiceRecognitionResponse:
    invoice_class = None
    try:
        if request.file_name.endswith('.pdf') and is_text_pdf(request.file_bytes):
            fragments = convert_pdf_bytes_to_fragments(request.file_bytes)
            invoice_class = InvoiceClass.TEXT_PDF
        else:
            fragments = ocr_process_image_to_fragments(request.file_name, request.file_bytes, ocr_method=request.ocr_method, use_cache=request.use_cache,preprocess=request.use_image_enhancement)
            invoice_class = classify_fragments(fragments)
            
        if invoice_class in SUPPORTED_CLASSES:
            invoice = reconstruct_invoice_from_fragments(fragments)
            response = InvoiceRecognitionResponse(status.HTTP_200_OK, invoice=invoice, invoice_class=invoice_class)
        else:
            error = 'The given file is not supported yet for invoice recognition!'
            response = InvoiceRecognitionResponse(status.HTTP_400_BAD_REQUEST, error_message=error, invoice_class=invoice_class)

        return response
    except Exception as ex:
        logger.exception('Could not process request: %s', request)
        return InvoiceRecognitionResponse(status.HTTP_500_INTERNAL_SERVER_ERROR, error_message=str(ex), invoice_class=invoice_class)
