#!/bin/bash
set -e

echo 'Starting server'
apt-get update


# installing required dependencies for https://github.com/Belval/pdf2image
echo 'Installing poppler-utils'
apt-get -y install poppler-utils

# Installing OpenCV dependencies
echo 'Installing OpenCV dependencies'
apt-get -y install libglib2.0-0


# gunicorn server startup
echo 'Starting gunicorn server'
gunicorn --bind=0.0.0.0 --timeout 600 --chdir rest rest_server:app