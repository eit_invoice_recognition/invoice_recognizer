import csv
import os
import io
import sys
import progressbar
import pickle
import codecs
from azure.cosmosdb.table.tableservice import TableService
from business_objects import Party, Address
from logger_setup import getLogger
from azure_storage import get_file_stream_from_cloud, store_entity_in_table, get_entity_from_cloud_table, create_table



class OptenDatabase(object):
    database = {} # not self, should be a class field, initialized only once
    OPTEN_TABLE_NAME = 'optentable'

    def __init__(self, 
        remote_opten: bool,
        opten_path: str):
        self.remote_opten = remote_opten
        self.logger = getLogger(__name__)
        progressbar.streams.wrap_stderr()

        if self.remote_opten:
            OPTEN_CSV_FILE = 'opten_limited.csv'
        else:
            OPTEN_CSV_FILE = os.path.join(opten_path, 'opten_limited.csv')
            if len(OptenDatabase.database) == 0:
                with open(OPTEN_CSV_FILE, encoding='UTF-8') as csvfile:
                    reader = csv.DictReader(csvfile)
                    self.logger.info('Reading opten data into memory, this may take a while...')
                    #global database
                    for row in progressbar.progressbar(reader):
                        address = Address(row['IRSZ'], row['HELY'], row['TERU'], row['TERJ'], row['HSZ'], row['EPLT'], row['LHAZ'], row['EMELET'], row['AJTO'])
                        OptenDatabase.database[row['ADOSZAM']] = Party((row['ADOSZAM'], None), row['NEV'], row['RNEV'], address)
        self.OPTEN_CSV_FILE = OPTEN_CSV_FILE

    def get_party_by_tax_number(self, tax_number: str) -> Party:
        retval = None
        if not self.remote_opten:
            # local stuff
            if tax_number not in OptenDatabase.database:
                self.logger.warning('Vat number %s not found in database!', tax_number)
                retval = Party((tax_number, None), None, None)
            else:
                retval = OptenDatabase.database[tax_number]
        else:
            # Azure stuff
            row_entity = get_entity_from_cloud_table(OptenDatabase.OPTEN_TABLE_NAME, tax_number)
            if row_entity is None:
                retval = None
            else:
                retval = pickle.loads(codecs.decode(row_entity.party.encode(), "base64"))

        return retval
        


    def refresh_table_in_storage(self):
        """
            This method uploads the Opten data to an azure table store. 
        """
        opten_csv_bytes = get_file_stream_from_cloud(self.OPTEN_CSV_FILE)
        create_table(OptenDatabase.OPTEN_TABLE_NAME)


        if opten_csv_bytes != None:
            with io.TextIOWrapper(opten_csv_bytes, 'UTF-8') as csvfile:
                line_count = len(csvfile.readlines())
                csvfile.seek(0)
                reader = csv.DictReader(csvfile)
                self.logger.info('Persisting opten data into Azure Cloud Table, this may take a while...')
                for row in progressbar.progressbar(reader, max_value=line_count):
                    address = Address(row['IRSZ'], row['HELY'], row['TERU'], row['TERJ'], row['HSZ'], row['EPLT'], row['LHAZ'], row['EMELET'], row['AJTO'])
                    # {'PartitionKey': 'tasksSeattle', 'RowKey': '001', 'description' : 'Take out the trash', 'priority' : 200}
                    party =  Party((row['ADOSZAM'], None), row['NEV'], row['RNEV'], address)
                    pickled_party = codecs.encode(pickle.dumps(party), "base64").decode()
                    row_entity = {'PartitionKey': '', 'RowKey': row['ADOSZAM'], 'party': pickled_party}
                    if not store_entity_in_table(OptenDatabase.OPTEN_TABLE_NAME, row_entity):
                        self.logger.error('Could not persist row: |%s|', row)
        else:
            self.logger.warning('Opten database file is not present! File does not exists: |%s|', self.OPTEN_CSV_FILE)


