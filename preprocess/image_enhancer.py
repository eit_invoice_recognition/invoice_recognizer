import cv2
import numpy as np
import io
from PIL import Image as im
import sys
import os
parent_path = os.path.abspath(os.path.join('..'))
if parent_path not in sys.path:
    sys.path.append(parent_path)
from invoiceparser.image_util import PIL_img_to_bytes

def image_adjust(gray_image):
    
    imin = np.min(gray_image)
    imax = np.max(gray_image)    
    gray_image = ((gray_image-imin)/(imax-imin))*255
    gray_image = gray_image.astype('uint8')
    return(gray_image)

def noise_removal(img_bytes: bytes) -> bytes:
    """
    Removes noise from the image. TODO: VERY SLOW
    """    
    original_img = im.open(io.BytesIO(img_bytes))
    original_img2 =np.array(original_img)
    gray_img = cv2.cvtColor(original_img2, cv2.COLOR_BGR2GRAY)  
                
    background_img = cv2.medianBlur(gray_img, 45)
    
    gray_img = gray_img.astype('double')
    background_img = background_img.astype('double')  

    foreground_img = background_img - gray_img   
    foreground_img[foreground_img<0] = 0;        
    
    corrected_img = 2*np.power(foreground_img, 0.5)  
    
    enhanced_img = image_adjust(corrected_img) 
    enhanced_img = im.fromarray(enhanced_img)
    enhanced_img.info = original_img.info

    return PIL_img_to_bytes(enhanced_img)


