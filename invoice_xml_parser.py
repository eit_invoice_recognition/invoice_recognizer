
# coding: utf-8

# In[16]:


import xml.etree.ElementTree as ET
from datetime import datetime
import urllib


# In[18]:


ns = {
    'xmlns': 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'ds' : 'http://www.w3.org/2000/09/xmldsig#',
    'cac' : 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    'cbc' : 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    'ext' : 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'n0' : 'urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2',
    'qdt' : 'urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2',
    'sac' : 'urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2',
    'sbc' : 'urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2',
    'udt' : 'urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2',
    'ccts-cct' : 'urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2',
    'xsi' : 'http://www.w3.org/2001/XMLSchema-instance',
    'schemaLocation' : 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 file:///C:/_INTERNAL_DEVS/UBL2.1/xsd/maindoc/UBL-Invoice-2.1.xsd'
}


# In[20]:


def get_all_used_namespaces(root):
    import re
    ns_values = set()
    for ele in root.iter():
        m = re.match('\{.*\}', ele.tag)
        ns_value = m.group(0)[1:-1]
        ns_values.add(ns_value)
    print(ns_values)


# In[23]:


def parse_xml(file_path):
    f = open(file_path)
    tree = ET.ElementTree(file=f)
    f.close()
    root = tree.getroot()
    return parse_xml_from_root(root)


# In[43]:


def parse_xml_from_root(root):

    invoice = None
    if 'PPLDocument' in root.tag:
        tmp = root.find('ClientDocument')
        invoice = tmp[0]
    elif 'Invoice' in root.tag:
        invoice = root

    invoice_id = (
        invoice
        .find('cbc:ID', ns)
        .text
    )
    invoice_date = (
        datetime
        .strptime(
            invoice
            .find('cbc:IssueDate', ns)
            .text,
            '%Y-%m-%d'
        )
        .strftime('%d-%m-%Y')
    )
    invoice_amount = (
        invoice
        .find('cac:LegalMonetaryTotal', ns)
        .find('cbc:PayableAmount', ns)
        .text
    )
    supplier_name = (
        invoice
        .find('cac:AccountingSupplierParty', ns)
        .find('cac:Party', ns)
        .find('cac:PartyName', ns)
        .find('cbc:Name', ns)
        .text
    )
    supplier_tax_number = (
        invoice
        .find('cac:AccountingSupplierParty', ns)
        .find('cac:Party', ns)
        .find('cac:PartyTaxScheme', ns)
        .find('cbc:CompanyID', ns)
        .text
    )
    supplier_address = (
        invoice
        .find('cac:AccountingSupplierParty', ns)
        .find('cac:Party', ns)
        .find('cac:PostalAddress', ns)
        .find('cac:AddressLine', ns)
        .find('cbc:Line', ns)
        .text
    )
    customer_name = (
        invoice
        .find('cac:AccountingCustomerParty', ns)
        .find('cac:Party', ns)
        .find('cac:PartyName', ns)
        .find('cbc:Name', ns)
        .text
    )
    customer_tax_number = (
        invoice
        .find('cac:AccountingCustomerParty', ns)
        .find('cac:Party', ns)
        .find('cac:PartyTaxScheme', ns)
        .find('cbc:CompanyID', ns)
        .text
    )
    customer_address = (
        invoice
        .find('cac:AccountingCustomerParty', ns)
        .find('cac:Party', ns)
        .find('cac:PostalAddress', ns)
        .find('cac:AddressLine', ns)
        .find('cbc:Line', ns)
        .text
    )
    

    d = {
        'invoice_id': invoice_id,
        'invoice_date': invoice_date,
        'invoice_amount': invoice_amount,
        'supplier_name' : supplier_name,
        'supplier_tax_number' : supplier_tax_number,
        'supplier_address' : supplier_address,
        'customer_name' : customer_name,
        'customer_tax_number': customer_tax_number,
        'customer_address' : customer_address
    }
    return d

