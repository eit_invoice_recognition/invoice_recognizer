
# Requirements for running the project on your machine

1. Install python dependencies of the project

    `pip install -r requirements.txt`

2. Install tesseract OCR engine

    https://github.com/tesseract-ocr/tesseract/wiki

3. Install poppler for PDF handling

    https://github.com/Belval/pdf2image 

    
