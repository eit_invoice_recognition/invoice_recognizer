import logging
import logging.handlers

LOG_FILENAME = 'ebiz_logs.out'

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')


file_handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when='D', utc=True, encoding='utf-8')
console_handler = logging.StreamHandler()

file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(formatter)
console_handler.setLevel(logging.DEBUG)

logger.addHandler(file_handler)
logger.addHandler(console_handler)

logger.info('Logger setup is completed! %s', logger)

def getLogger(logger_name: str):
    return logging.getLogger(logger_name)