from typing import List
from functional import seq
import numpy as np
import pickle
import io
import os
from business_objects import InvoiceClass
from business_objects import *
from logger_setup import getLogger


logger = getLogger(__name__)

dt_classifier_dump = os.path.join(os.path.dirname(__file__), 'dt_classifier.dump')

with io.open(dt_classifier_dump, 'rb') as f:
    classifier = pickle.load(f)

# Represents relevant features of an invoice that can be used for classification
class InvoiceFeatures(object):

    def __init__(self,
        min_x: int,
        max_x: int,
        min_y: int,
        max_y: int,
        width_to_height_ratio: float,
        number_count: int,
        line_count: int,
        wordbox_count: int,
        different_x_values_count: int,
        different_y_values_count: int,
        avg_fragment_count_per_line: float,
        max_box_length: int,
        avg_box_length: int = None,
        pages: int = None,
        class_number: int = None):
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.width_to_height_ratio = width_to_height_ratio
        self.number_count = number_count
        self.line_count = line_count
        self.wordbox_count = wordbox_count
        self.different_x_values_count = different_x_values_count
        self.different_y_values_count = different_y_values_count
        self.avg_fragment_count_per_line = avg_fragment_count_per_line
        self.max_box_length = max_box_length
        self.avg_box_length = avg_box_length
        self.class_number = class_number
    
    def values(self):
        dictionary = vars(self)
        return np.array([dictionary[key] for key in sorted(dictionary.keys()) if dictionary[key] != None]).reshape(1, -1)

def __number_of_digits_in_string(string: str) -> int:
    return len([c for c in string if c.isdigit()])

def extract_features_of_fragments(fragments: List[Fragment]) -> InvoiceFeatures:
    number_count = sum([__number_of_digits_in_string(fragment.text) for fragment in fragments])
    wordbox_count = len(fragments)
    different_x_values_count = len(set([fragment.x for fragment in fragments]))
    different_y_values_count = len(set([fragment.y for fragment in fragments]))
    fragment_lines_groups = seq(fragments).group_by(lambda fragment: round(fragment.y, -1)).list()
    line_count = len(fragment_lines_groups)
    avg_fragment_count_per_line = np.mean([len(line_fragments[1]) for line_fragments in fragment_lines_groups])
    min_x_fragment = seq(fragments).min_by(lambda fragment: fragment.x)
    max_x_fragment = seq(fragments).max_by(lambda fragment: fragment.x)
    min_y_fragment = seq(fragments).min_by(lambda fragment: fragment.y)
    max_y_fragment = seq(fragments).max_by(lambda fragment: fragment.y)
    min_x = min_x_fragment.x
    max_x = max_x_fragment.x
    min_y = min_y_fragment.y
    max_y = max_y_fragment.y
    max_box_length = max([len(fragment.text) for fragment in fragments])
    width = max_x_fragment.x + max_x_fragment.width - min_x
    height = max_y_fragment.y + max_y_fragment.height - min_y_fragment.y 
    width_to_height_ratio = width / height


    return InvoiceFeatures(min_x, max_x, min_y, max_y, width_to_height_ratio, number_count, line_count, wordbox_count, different_x_values_count, different_y_values_count, avg_fragment_count_per_line, max_box_length)


def classify_fragments(fragments: InvoiceDocument) -> InvoiceClass:
    global classifier
    if len(fragments.fragment_list) == 0:
        return InvoiceClass.OTHER
    
    features = extract_features_of_fragments(fragments.fragment_list)
    class_number = classifier.predict(features.values())
    return InvoiceClass(class_number)