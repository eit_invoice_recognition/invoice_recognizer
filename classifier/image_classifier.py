import numpy
from keras.preprocessing import image
from PIL import Image as im
import io
from keras.applications.vgg16 import preprocess_input
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump, load
from business_objects import InvoiceClass

if 'classifier' in locals():
    print('KNN model is loaded')
else:
    classifier = load('KNNClas.joblib');

if 'my_model' in locals():
    print('Deep Model is loaded')
else:
    my_model= load('Deepmodel.joblib');
   
def Pridict_image(image_bytes):
    global classifier
    global my_model    
    img = im.open(io.BytesIO(image_bytes))
    img = img.resize([224,224])
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = preprocess_input(img_data)
    feature = my_model.predict(img_data)
    feature = numpy.array(feature)
    y_pred = int(classifier.predict(feature))
    return InvoiceClass(y_pred)


