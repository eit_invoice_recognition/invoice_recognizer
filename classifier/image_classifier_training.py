import xlsxwriter
from os import path
import os
from glob import glob  
import numpy
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import VGG16
import numpy as np
from keras.layers import Dense, Activation
from keras import Model
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump, load




def save_features_excel(fe):
    workbook = xlsxwriter.Workbook('arrays2.xlsx')
    worksheet = workbook.add_worksheet()
    
    array = fe
    row = 0
    
    for col, data in enumerate(array):
        worksheet.write_column(row, col, data)
    
    workbook.close()
    
def find_ext(dr, ext):
    return glob(path.join(dr,"*.{}".format(ext)))

      
dirnames=os.listdir(".")
stdirnames=(dirnames[:3])
indirnames = [int(i) for i in stdirnames]
fe=[]

model = VGG16(weights='imagenet', include_top=True )
x = Dense(4096, activation='tanh', name='predictions')(model.layers[-3].output)
my_model = Model(input=model.input, output=x)
my_model.summary()
for i in range(1,4):
    npath=os.path.join(".",stdirnames[i-1])
    imlist=find_ext(npath,"jpeg")
    imlist=imlist+find_ext(npath,"jpg")
    for j in range(0,len(imlist)):
        filename=imlist[j]
        filename=filename[2:]    
        img = image.load_img(filename, target_size=(224, 224))
        img_data = image.img_to_array(img)
        img_data = np.expand_dims(img_data, axis=0)
        img_data = preprocess_input(img_data)
        feature = my_model.predict(img_data)[0] 
        feature = numpy.append(feature,i)
        fe.append(feature)
        print(i,j)

#saving features (with labels) in Excel file named array2.xlsx in the same folder for Weka testing puropses
#the results not as good as Matlabs Alex, cuz there are no norm layers in pytorch library for pre-trained Alexnet yet 
#save_features_excel(fe);

print('Training KNN')
fe=numpy.array(fe); 
Training=fe[:,:-1]
Labels=fe[:,4096]
classifier = KNeighborsClassifier(n_neighbors=1)
classifier.fit(Training, Labels);
print('End of training');   

dump(classifier, 'KNNClas.joblib');
dump(my_model, 'Deepmodel.joblib');
print('Models are saved!');

    
