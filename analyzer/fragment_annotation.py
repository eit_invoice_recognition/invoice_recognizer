from typing import List, Callable, Union, Tuple
from functional import seq
from datetime import date
import numpy
import os
import sys
import numpy as np
parent_path = os.path.abspath(os.path.join('..'))
if parent_path not in sys.path:
    sys.path.append(parent_path)
from business_objects import *
from analyzer.text_manipulation import to_ascii, clean_numberstring_of_ocr_errors, get_float_from_text, contains_digits,\
    contains_date, get_date_from_text, min_edit_distance_ratio_singleword, min_edit_distance_ratio_multiword, get_id_from_text,\
    contains_tax_number, get_tax_number_from_text, contains_strict_tax_number, get_strict_tax_number_from_text, contains_currency, contains_decimal_point,\
    sanitize_whitespace
from business_objects import Invoice, Party, InvoiceClass
from logger_setup import getLogger
import opten.opten_database as opten_database
import config
from abc import ABC, abstractmethod
import pandas as pd
from enum import IntEnum
import unidecode



INVOICE_NUMBER_KEYWORDS = ["szamlaszam", "sorszam", "kozlemeny", "azonosito", "bizonylatszam", "bizonyszam"] # topright corner without any keyword also works? Contains numbers every time?
PAYABLE_KEYWORDS = ["fizetendo", "vegosszeg", "vegosszesen", "osszesen", "osszeg", "mindosszesen"]
# 'Teljesites' is present all the time. This comes before date of invoice, as sometimes payment date is 'teljesites kelte' 
PAYMENT_DATE_KEYWORDS = ["teljesites", "telj"] 
DATE_OF_INVOICE_KEYWORDS = ["kelte", "kiallitas", "bizonylatdatum", "idopont"]
PAYMENT_DUE_KEYWORDS = ["hatarido", "esedekesseg"]

PAYMENT_METHOD_KEYWORDS = ["fizetesi mod"]
PAYMENT_METHOD_VALUE_KEYWORDS = ["atutalas", "bankkartya"]

# Possible edge case: 30502-HMDI_000000023-ebiz_handmade_document_in.pdf1.jpg, 
SELLER_KEYWORDS = ["kibocsato", "szallito", "elado", "kiallito", "felado", "szolgaltato"]
BUYER_KEYWORDS = ["vevo", "cimzett", "vasarlo", "ugyfel"]

logger = getLogger(__name__)
opten_db = opten_database.OptenDatabase(config.REMOTE_OPTEN, config.OPTEN_PATH)

EDIT_DISTANCE_EQUALITY_TRESHOLD = 0.2
HORIZONTAL_MARGIN = 0.3


def _any_fragment(fragment: Fragment, of: Fragment, y_direction: YDirection) -> bool:
    return True

def _fragment_distance(aFragment: Fragment, bFragment: Fragment) -> float:
    a = numpy.array((aFragment.x, aFragment.y))
    b = numpy.array((bFragment.x, bFragment.y))

    return numpy.linalg.norm(a-b)




def _are_on_same_page(fragment_a: Fragment, fragment_b: Fragment) -> bool:
    return fragment_a.page == fragment_b.page


def _find_fragment_containing_keyword(fragments: List[Fragment], keyword: str) -> Fragment:
    return next((fragment for fragment in fragments if keyword in fragment.text.lower()), None)


def _find_fragment_containing_any_keyword(fragments: List[Fragment], keywords: List[str]) -> Fragment:
    return next((fragment for fragment in fragments if \
                            any(keyword in to_ascii(fragment.text).lower() for keyword in keywords)), None)

def _find_fragment_by_keyword(fragments: InvoiceDocument, keywords: List[str],\
                                keyword_edit_distance_function: Callable[[str, List[str]], float] = min_edit_distance_ratio_singleword)\
                                    -> Fragment:
    edit_distance_keyword_fragment = seq(fragments.fragment_list)\
                        .map(lambda fragment: [keyword_edit_distance_function(fragment.text, keywords), fragment])\
                        .sorted(key=lambda fragment: fragment[0])\
                        .first()
    
    if edit_distance_keyword_fragment[0] > EDIT_DISTANCE_EQUALITY_TRESHOLD:
        logger.info('Unable to find a close enough keyword fragment for the keywords |%s| based on fragments: |%s|', keywords, '\n\t'.join(str(fragment) for fragment in fragments.fragment_list))
        logger.info('Closest fragment is |%s| with an edit distance ratio of: |%f|', edit_distance_keyword_fragment[1], edit_distance_keyword_fragment[0])
        return None
    
    return edit_distance_keyword_fragment[1]


def _remove_unnecessary_distubing_parts(fragments: InvoiceDocument) -> None:
    """
        The invoice items are not needed however their headers overlap with some needed invoice level keywords. 
        Thus we remove all the invoice item list header fragments. The values can stay.
        The list item header can be there multiple times as the list might go on for multiple pages.
    
        We identify the invoice item headers by looking for a line which contains the following: [Mennyiség], [termék, szolgáltatás, megnevezés, tétel, terméknév], [egységár]
    """
    pass

def _find_tax_number_by_value(fragments: InvoiceDocument) -> str:
    tax_number = next(seq(fragments.fragment_list)\
                    .filter(lambda fragment: contains_strict_tax_number(fragment.text))\
                    .distinct_by(lambda fragment: get_strict_tax_number_from_text(fragment.text))\
                    .order_by(lambda fragment: fragment.x)\
                    .__iter__(), None)
    if tax_number == None:
        tax_number = next(seq(fragments.fragment_list)\
                        .filter(lambda fragment: contains_tax_number(fragment.text))\
                        .distinct_by(lambda fragment: get_tax_number_from_text(fragment.text))\
                        .order_by(lambda fragment: fragment.x)\
                        .__iter__(), None)
    if tax_number != None:
        tax_number = (get_tax_number_from_text(tax_number.text), tax_number)

    return tax_number

def _find_invoice_parties(fragments: InvoiceDocument) -> (Party, Party):
    """
        This method tries to reconstruct the 2 invoice parties (Seller, Buyer) from the fragments. 
        First the method tries to find the 2 tax numbers. 
        If there is only one tax number we consider it as the Seller's tax number. 
        If there are 2 or more vat numbers found, we will sort them by their distance to the Buyer fragment (identified by the BUYER_KEYWORDS). 
            The closes VAT number belongs to the buyer, the 2nd closes to the seller.
        Using the tax numbers we query the Opten database for further information about the 2 companies.
    """
    buyer_fragment = _find_fragment_by_keyword(fragments, BUYER_KEYWORDS)
    if buyer_fragment == None:
        buyer_fragment = seq(fragments.fragment_list)\
                            .filter(lambda fragment: fragment.page == 1)\
                            .max_by(lambda fragment: fragment.x)
        logger.info('No buyer fragment found! Defaulting to a fragment on the right of the first page.')

    logger.info('Buyer fragment: |%s|', buyer_fragment)


    seller = None
    buyer = None
    tax_number_fragments = seq(fragments.fragment_list)\
                        .filter(lambda fragment: contains_tax_number(fragment.text))\
                        .distinct_by(lambda fragment: get_tax_number_from_text(fragment.text))\
                        .order_by(lambda fragment: np.linalg.norm(np.array([fragment.x, fragment.y]) - numpy.array([buyer_fragment.x,buyer_fragment.y])))\
                        .to_list()


    if len(tax_number_fragments) == 0:
        logger.info("Could not find any TAX numbers based on fragments: |%s|", '\n\t'.join(str(fragment) for fragment in fragments.fragment_list))
    elif len(tax_number_fragments) == 1:
        logger.debug("TAX number(s): |%s|", '\n\t'.join(str(fragment) for fragment in tax_number_fragments))
        seller = opten_db.get_party_by_tax_number(get_tax_number_from_text(tax_number_fragments[0].text))
        seller.tax_number = (seller.tax_number[0], tax_number_fragments[0]) # the cache does not contain the corresponding fragment so we add it
        fragments.fragment_list.remove(tax_number_fragments[0])
    elif len(tax_number_fragments) >= 2:
        if len(tax_number_fragments) > 2:
            logger.warn("Found more than 2 TAX numbers! Taking the first two and ignoring the rest. Matches: |%s|", '\n\t'.join(str(fragment) for fragment in tax_number_fragments))
        else:
            logger.debug("Found 2 TAX numbers: |%s|", '\n\t'.join(str(fragment) for fragment in tax_number_fragments))
        buyer = opten_db.get_party_by_tax_number(get_tax_number_from_text(tax_number_fragments[0].text))
        seller = opten_db.get_party_by_tax_number(get_tax_number_from_text(tax_number_fragments[1].text))
        if buyer != None:
            buyer.tax_number = (buyer.tax_number[0], tax_number_fragments[0])
            fragments.fragment_list.remove(tax_number_fragments[0])
        if seller != None:
            seller.tax_number = (seller.tax_number[0], tax_number_fragments[1])
            fragments.fragment_list.remove(tax_number_fragments[1])

    return seller, buyer

def _top_right_corner(fragments: InvoiceDocument) -> Tuple[int, int]:
    max_x = max([fragment.x for fragment in fragments.fragment_list])
    if fragments.y_direction == YDirection.UP:
        max_y = max([fragment.y for fragment in fragments.fragment_list])
    else:
        max_y = min([fragment.y for fragment in fragments.fragment_list])

    return np.array([max_x, max_y])

def _find_value_by_location(fragments: InvoiceDocument, 
                                anchor_locator: Callable[[InvoiceDocument], Tuple[int, int]],
                                value_identifier: Callable[[str], bool],
                                value_converter: Callable[[str], Union[float, str, date]]) -> Tuple[str, Fragment]:
    anchor = anchor_locator(fragments)
    title_keyword_fragment = _find_fragment_by_keyword(fragments, ['szamla'])
    if title_keyword_fragment == None:
        title_keyword_fragment = fragments.fragment_list[0]
    
    value_fragment = next(seq(fragments.fragment_list)\
                        .filter(lambda fragment: _are_on_same_page(fragment, title_keyword_fragment))\
                        .filter(lambda fragment: value_identifier(fragment.text))\
                        .order_by(lambda fragment: np.linalg.norm(np.array([fragment.x, fragment.y]) - anchor))\
                        .__iter__(), None)
    if value_fragment == None:
        logger.info('Could not find any matching value by location! anchor: |%s|, title_keyword_fragment: |%s|', anchor, title_keyword_fragment)
        return None


    logger.debug('Trying to get value from |%s| using converter: |%s|', value_fragment.text, str(value_converter))
    return (value_converter(value_fragment.text), value_fragment)



class FragmentAnnotationBase(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def reconstruct_invoice_from_fragments(self, fragments: InvoiceDocument) -> Invoice:
        pass


class FragmentType(IntEnum):
    Other = 0
    InvoiceNumber = 1
    IssueDate = 2
    TransactionDate = 3
    DueDate = 4
    SellerTaxNumber = 5
    CustomerTaxNumber = 6
    TotalPayable = 7


class FragmentAnnotationML(FragmentAnnotationBase):

    ALL_KEYWORDS = ["atutalas", "azonosito", "bankkartya", "bizonylatdatum", "bizonylatszam", "bizonyszam", "cimzett", "dijbekero", "elado", "esedekesseg", "felado", "fizetendo", "fizetesi mod", "fizetes mod", "hatarido", "idopont", "kelte", "kiallitas", "kiallito", "kibocsato", "kozlemeny", "mindosszesen", "osszeg", "osszesen", "sorszam", "szallito", "szam", "szamlaszam", "szolgaltato", "telj.", "teljesites", "ugyfel", "vasarlo", "vegosszeg", "vegosszesen", "vevo"]

    def __init__(self):
        # load model
        pass

    def _enrich_fragments_for_ML(self, document: InvoiceDocument) -> pd.DataFrame:
        """
            Adds the following features to the fragments:
            id
            X
            Y
            width
            height
            X_norm #normalized relative to document [0,1]
            Y_norm #normalized relative to document [0,1]
            doc_y_direction
            invoice_class
            page
            fragment_source
            page_percent
            width_per_docwidth
            height_per_docheight
            doc_min_x
            doc_min_y
            doc_max_x
            doc_max_y
            doc_width
            doc_height
            docwidth_per_docheight
            # text properties
            text_length
            matches_date_regex
            matches_taxnr_regex
            matches_currency_regex
            matches_decimalpoint_regex
            number_count
            whitespace_count
            character_count
            slash_count
            dot_count
            comma_count
            colon_count
            percent_count
            bracket_count
            # following features answer where is the nearest fragment containing the given important keyword. 
            # The xy distances are normalized to [0, 1] with dividing them by doc widht and height. 
            # If given keyword is not found, value is -10
            atutalas_x_dir
            atutalas_y_dir
            azonosito_x_dir
            azonosito_y_dir
            bankkartya_x_dir
            bankkartya_y_dir
            bizonylatdatum_x_dir
            bizonylatdatum_y_dir
            bizonylatszam_x_dir
            bizonylatszam_y_dir
            bizonyszam_x_dir
            bizonyszam_y_dir
            cimzett_x_dir
            cimzett_y_dir
            dijbekero_x_dir
            dijbekero_y_dir
            elado_x_dir
            elado_y_dir
            esedekesseg_x_dir
            esedekesseg_y_dir
            felado_x_dir
            felado_y_dir
            fizetendo_x_dir
            fizetendo_y_dir
            fizetesi mod_x_dir
            fizetesi mod_y_dir
            fizetes mod_x_dir
            fizetes mod_y_dir
            hatarido_x_dir
            hatarido_y_dir
            idopont_x_dir
            idopont_y_dir
            kelte_x_dir
            kelte_y_dir
            kiallitas_x_dir
            kiallitas_y_dir
            kiallito_x_dir
            kiallito_y_dir
            kibocsato_x_dir
            kibocsato_y_dir
            kozlemeny_x_dir
            kozlemeny_y_dir
            mindosszesen_x_dir
            mindosszesen_y_dir
            osszeg_x_dir
            osszeg_y_dir
            osszesen_x_dir
            osszesen_y_dir
            sorszam_x_dir
            sorszam_y_dir
            szallito_x_dir
            szallito_y_dir
            szam_x_dir
            szam_y_dir
            szamlaszam_x_dir
            szamlaszam_y_dir
            szolgaltato_x_dir
            szolgaltato_y_dir
            telj._x_dir
            telj._y_dir
            teljesites_x_dir
            teljesites_y_dir
            ugyfel_x_dir
            ugyfel_y_dir
            vasarlo_x_dir
            vasarlo_y_dir
            vegosszeg_x_dir
            vegosszeg_y_dir
            vegosszesen_x_dir
            vegosszesen_y_dir
            vevo_x_dir
            vevo_y_dir
        """
        fragment_features = []
        doc_width = float(document.max_x - document.min_x)
        doc_height = float(document.max_y - document.min_y)
        if doc_height <= 0 or doc_width <= 0:
            raise ValueError(f'Document Dimensionality Error! Height: |{doc_height}|, Width: |{doc_width}|')

        #Store a list of fragments for each keyword
        keyword_map = {keyword:[] for keyword in FragmentAnnotationML.ALL_KEYWORDS}
        for fragment in document.fragment_list:
            text = sanitize_whitespace(fragment.text.lower())
            text = text.lower()
            text = unidecode.unidecode(text)
            for keyword in FragmentAnnotationML.ALL_KEYWORDS:
                if keyword in text:
                    keyword_map[keyword].append(fragment)

        for fragment in document.fragment_list:
            feature_map = {}
            feature_map['id'] = fragment.id
            feature_map['X'] = fragment.x
            feature_map['Y'] = fragment.y
            feature_map['width'] = fragment.width
            feature_map['height'] = fragment.height
            feature_map['X_norm'] = fragment.x / doc_width
            feature_map['Y_norm'] = fragment.y / doc_width
            feature_map['doc_y_direction'] = int(document.y_direction)
            feature_map['invoice_class'] = document.invoice_class
            feature_map['page'] = fragment.page
            feature_map['fragment_source'] = document.fragment_source
            feature_map['page_percent'] = fragment.page / document.pages
            feature_map['width_per_docwidth'] = fragment.width / doc_width
            feature_map['height_per_docheight'] = fragment.height / doc_height
            feature_map['doc_min_x'] = document.min_x
            feature_map['doc_min_y'] = document.min_y
            feature_map['doc_max_x'] = document.max_x
            feature_map['doc_max_y'] = document.max_y
            feature_map['doc_width'] = doc_width
            feature_map['doc_height'] = doc_height
            feature_map['docwidth_per_docheight'] = doc_width / doc_height
            # text properties
            feature_map['text_length'] = len(fragment.text)
            feature_map['matches_date_regex'] = int(contains_date(fragment.text))
            feature_map['matches_taxnr_regex'] = int(contains_tax_number(fragment.text))
            feature_map['matches_currency_regex'] = int(contains_currency(fragment.text))
            feature_map['matches_decimalpoint_regex'] = int(contains_decimal_point(fragment.text))
            feature_map['number_count'] = 0
            feature_map['whitespace_count'] = 0
            feature_map['character_count'] = 0
            feature_map['slash_count'] = 0
            feature_map['dot_count'] = 0 
            feature_map['comma_count'] = 0
            feature_map['colon_count'] = 0
            feature_map['semicolon_count'] = 0
            feature_map['percent_count'] = 0
            feature_map['bracket_count'] = 0
            for ch in fragment.text:
                if ch.isdigit():
                    feature_map['number_count'] += 1
                elif ch.isspace():
                    feature_map['whitespace_count'] += 1
                elif ch.isalpha():
                    feature_map['character_count'] += 1
                elif ch in '\\/':
                    feature_map['slash_count'] += 1
                elif ch == '.':
                    feature_map['dot_count'] += 1
                elif ch == ',':
                    feature_map['comma_count'] += 1
                elif ch == ':':
                    feature_map['colon_count'] += 1
                elif ch == ';':
                    feature_map['semicolon_count'] += 1
                elif ch == '%':
                    feature_map['percent_count'] += 1
                elif ch in '[]{}()':
                    feature_map['bracket_count'] += 1
            
            for keyword in FragmentAnnotationML.ALL_KEYWORDS:
                closest = next(iter(sorted([(_fragment_distance(fragment, keyword_fragment), keyword_fragment) for keyword_fragment in keyword_map[keyword]]
                                ,key = lambda pair: (pair[0], pair[1].id))), None)
                if closest == None:
                    x_dir = -10
                    y_dir = -10
                else:
                    x_dir = (closest[1].x - fragment.x) / doc_width
                    y_dir = (closest[1].y - fragment.y) / doc_height

                feature_map[keyword + '_x_dir'] = x_dir
                feature_map[keyword + '_y_dir'] = y_dir

            fragment_features.append(feature_map)

        return pd.DataFrame(fragment_features)

    def reconstruct_invoice_from_fragments(self, fragments: InvoiceDocument) -> Invoice:
        pass

class FragmentAnnotationRuleBased(FragmentAnnotationBase):

    def __init__(self):
        # load model
        pass

    def __is_in_same_line_right_of(self, fragment: Fragment, of: Fragment, y_direction: YDirection) -> bool:
        return of.y - of.height / 2 < fragment.y < of.y + of.height / 2 and of.x < fragment.x

    def __is_in_same_column_under(self, fragment: Fragment, of: Fragment, y_direction: YDirection) -> bool:
        """
            The first part of the expression is checking if the fragment's center point is between the column defined
            by the left edge of the 'of' fragment and the right edge of the 'of' fragment with an allowed error of HORIZONTAL_MARGIN
        """
        if y_direction ==  YDirection.UP:
            return of.x - of.width * HORIZONTAL_MARGIN < fragment.x + fragment.width / 2 < of.x + of.width + of.width * HORIZONTAL_MARGIN\
                        and of.y > fragment.y
        else:
            return of.x - of.width * HORIZONTAL_MARGIN < fragment.x + fragment.width / 2 < of.x + of.width + of.width * HORIZONTAL_MARGIN\
                        and of.y < fragment.y


    def __under_or_right_of(self, fragment: Fragment, of: Fragment, y_direction: YDirection) -> bool:
        return self.__is_in_same_column_under(fragment, of, y_direction) or self.__is_in_same_line_right_of(fragment, of, y_direction)


    def __find_value_by_keyword(self, fragments: InvoiceDocument, keywords: List[str],\
                value_identifier: Callable[[str], bool],\
                value_converter: Callable[[str], Union[float, str, date]],\
                value_locator: Callable[[Fragment, Fragment, YDirection], bool] = __under_or_right_of,\
                keyword_edit_distance_function: Callable[[str, List[str]], float] = min_edit_distance_ratio_singleword,\
                force_value_extraction_from_keyword: bool = True
                ) -> Union[float, str, date, None]:
        keyword_fragment = _find_fragment_by_keyword(fragments, keywords, keyword_edit_distance_function)
        if keyword_fragment == None:
            return None
        
        fragments.fragment_list.remove(keyword_fragment)


        value_fragment = next(seq(fragments.fragment_list)\
                            .filter(lambda fragment: _are_on_same_page(fragment, keyword_fragment))\
                            .filter(lambda fragment: value_locator(fragment, keyword_fragment, fragments.y_direction))\
                            .filter(lambda fragment: value_identifier(fragment.text))\
                            .sorted(key=lambda fragment: _fragment_distance(keyword_fragment, fragment))\
                            .__iter__(), None)
        if value_fragment == None:
            logger.info('Unable to find matching value to the right of |%s| using identifier: |%s| based on fragments: |%s|', str(keyword_fragment), \
                                    str(value_identifier), '\n\t'.join(str(fragment) for fragment in fragments.fragment_list))
            if not force_value_extraction_from_keyword:
                return None
            text = keyword_fragment.text
            value_fragment = keyword_fragment
        else:
            text = keyword_fragment.text + ' ' + value_fragment.text
            fragments.fragment_list.remove(value_fragment)
        
        logger.debug('Trying to get value from |%s| using converter: |%s|', text, str(value_converter))
        return (value_converter(text), value_fragment)

    def _find_invoice_parties_new(self, fragments: InvoiceDocument) -> (Party, Party):
        """
            This method tries to find the invoice parties based on the tax numbers using Keyword based lookup.
            First we try to look for strict tax numbers with dashes, then fallback to tax numbers without dashes.
            For the seller if keyword based lookup fails we fallback to value based lookup.
        """
        # First by keyword
        seller_fragment = self.__find_value_by_keyword(fragments, SELLER_KEYWORDS, contains_strict_tax_number, get_strict_tax_number_from_text, value_locator=_any_fragment, force_value_extraction_from_keyword=False)
        if seller_fragment == None: 
            seller_fragment = self.__find_value_by_keyword(fragments, SELLER_KEYWORDS, contains_tax_number, get_tax_number_from_text, value_locator=_any_fragment, force_value_extraction_from_keyword=False)

        buyer_fragment = self.__find_value_by_keyword(fragments, BUYER_KEYWORDS, contains_strict_tax_number, get_strict_tax_number_from_text, value_locator=_any_fragment, force_value_extraction_from_keyword=False)
        if buyer_fragment == None:
            buyer_fragment = self.__find_value_by_keyword(fragments, BUYER_KEYWORDS, contains_tax_number, get_tax_number_from_text, value_locator=_any_fragment, force_value_extraction_from_keyword=False)


        # Then by value
        if seller_fragment == None:
            seller_fragment = _find_tax_number_by_value(fragments)
        if buyer_fragment == None:
            buyer_fragment = _find_tax_number_by_value(fragments)

        seller = None
        buyer = None
        if seller_fragment != None:
            seller = opten_db.get_party_by_tax_number(seller_fragment[0])
        if buyer_fragment != None:
            buyer = opten_db.get_party_by_tax_number(buyer_fragment[0])
        

        return seller, buyer

    def reconstruct_invoice_from_fragments(self, fragments: List[Fragment]) -> Invoice:
        """
        Rule based fragment annotation 

        Tries to reconstruct the Invoice object from the available fragments. 

        After a piece of information has been extracted we remove the related fragments from the list.
        The main logic is based on finding keywords in the document that identify components of the invoice.
        After we find a keyword we look for values in the same line that are right of the keyword and extract the one
        that maches the format of this component. 

        """
        if len(fragments.fragment_list) == 0:
            return Invoice()

        _remove_unnecessary_distubing_parts(fragments)
        seller, buyer = self._find_invoice_parties_new(fragments)
        price = self.__find_value_by_keyword(fragments, PAYABLE_KEYWORDS, contains_digits, get_float_from_text)
        date_of_invoice = self.__find_value_by_keyword(fragments, DATE_OF_INVOICE_KEYWORDS, contains_date, get_date_from_text)
        payment_date = self.__find_value_by_keyword(fragments, PAYMENT_DATE_KEYWORDS, contains_date, get_date_from_text)
        payment_due = self.__find_value_by_keyword(fragments, PAYMENT_DUE_KEYWORDS, contains_date, get_date_from_text)
        invoice_number = self.__find_value_by_keyword(fragments, INVOICE_NUMBER_KEYWORDS, contains_digits, get_id_from_text)
        payment_method = self.__find_value_by_keyword(fragments, PAYMENT_METHOD_KEYWORDS, lambda text: min_edit_distance_ratio_singleword(text, PAYMENT_METHOD_VALUE_KEYWORDS) < 0.2,\
                            lambda text: next((word for word in text.split() if min_edit_distance_ratio_singleword(word, PAYMENT_METHOD_VALUE_KEYWORDS) < 0.2), text),\
                            keyword_edit_distance_function=min_edit_distance_ratio_multiword)

        

            
        # In the case we don't have an invoice keyword
        if invoice_number == None:
            logger.info('Falling back to top right location of invoice number')
            invoice_number = _find_value_by_location(fragments, _top_right_corner, contains_digits, get_id_from_text)

        return Invoice(invoice_number=invoice_number, price=price, issuer=seller, customer=buyer,
                            date_of_invoice=date_of_invoice, payment_date=payment_date, payment_due=payment_due, payment_method=payment_method)