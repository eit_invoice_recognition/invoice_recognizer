#pip install unidecode
import unidecode
from logger_setup import getLogger
import re
from datetime import date
from typing import List
import numpy as np
from itertools import combinations

OCR_ERROR_DICT_FOR_NUMBERS = {
    "O": "0",
    "o": "0",
    "I": "1",
    "Z": "2",
    ",": ".",
    "—": "-"
}

date_matcher = re.compile(r'\d{4}[./-]\d{2}[./-]\d{1,2}')
# For matching correctly formed tax numbers
tax_number_matcher = re.compile(r'[1-9]{1}\d{7}-[1-5]{1}-\d{2}')
# For matching tax numbers that have dashes omitted for some reason. Example: 20400-HMDI_000000049-ebiz_handmade_document_in.pdf1.jpg
tax_number_matcher_no_dashes = re.compile(r'[1-9]{1}\d{7}[1-5]{1}\d{2}')
decimal_point_matcher = re.compile(r'\d[.,]\d')

logger = getLogger(__name__)

def clean_whitespace(string: str) -> str:
    return re.sub(r'\s+', '', string)


def sanitize_whitespace(string: str) -> str:
    return re.sub(r'\s+', ' ', string).strip()

def clean_numberstring_of_ocr_errors(string: str) -> str:
    for error_char in OCR_ERROR_DICT_FOR_NUMBERS:
        string = string.replace(error_char, OCR_ERROR_DICT_FOR_NUMBERS[error_char])

    return string

def min_edit_distance_ratio_singleword(text: str, keywords: List[str]) -> float:
    minimum = 1.0
    
    for word in text.split():
        for keyword in keywords:
            distance = __levenshtein(clean(word), keyword) / len(word)
            if minimum > distance:
                minimum = distance

    return minimum

def min_edit_distance_ratio_multiword(text: str, expressions: List[str]) -> float:
    minimum = 1.0

    words = text.split()

    for expression in expressions:
        n = len(expression.split())
        for combination in combinations(words, n):
            distance = __levenshtein(clean(' '.join(combination))[:len(expression)], expression) / len(expression)
            if minimum > distance:
                minimum = distance

    return minimum

def clean(string: str) -> str:
    return to_ascii(string).replace(":", "").lower()

def __levenshtein(seq1, seq2):  
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros ((size_x, size_y))
    for x in range(size_x):
        matrix [x, 0] = x
    for y in range(size_y):
        matrix [0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x-1] == seq2[y-1]:
                matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                )
            else:
                matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                )

    return (matrix[size_x - 1, size_y - 1])

def get_float_from_text(text: str) -> float:
    clean_text = clean_numberstring_of_ocr_errors(text)
    number = ''.join(c for c in clean_text if c.isdigit() or c == '.')

    try:
        return float(number)
    except ValueError:
        logger.warn('Could not extract float from |%s|', text)
        return None


def get_date_from_text(text: str, warn_if_not_found: bool = True) -> date:
    clean_text = clean_numberstring_of_ocr_errors(text)
    date_text = clean_text.replace(' ', '')
    match = date_matcher.search(date_text)

    if match == None:
        if warn_if_not_found:
            logger.warn('Could not extract date from |%s|', text)
        return None

    date_parts = match.group(0).replace('/', '.').replace('-', '.').split(".")

    try:
        return date(int(date_parts[0]), int(date_parts[1]), int(date_parts[2]))
    except ValueError as x:
        if warn_if_not_found:
            logger.warn('Could not extract date from |%s|', text)
            logger.warn(x)
        return None

def get_id_from_text(text: str) -> str:
    return next((word for word in re.split(r'[\s:;]+', text) if contains_digits(word)), None)

def get_correctly_formatted_tax_number_from_text(text: str) -> str:
    clean_text = clean_numberstring_of_ocr_errors(text)
    tax_number_text = clean_text.replace(' ', '')# ''.join(c for c in clean_text if c.isdigit() or c == '-')
    match = tax_number_matcher.search(tax_number_text)

    if match == None:
        return None

    return match.group(0)


def get_numbers_only_tax_number_from_text(text: str) -> str:
    clean_text = clean_numberstring_of_ocr_errors(text)
    tax_number_text = clean_text.replace(' ', '') #''.join(c for c in clean_text if c.isdigit())
    match = tax_number_matcher_no_dashes.search(tax_number_text)
    
    if match == None:
        return None


    return match.group(0)
    #vat_raw = match.group(0)
    #return vat_raw[:8] + '-' + vat_raw[-3] + '-' + vat_raw[-2:] #23584497241  -> 23584497-2-41

def get_strict_tax_number_from_text(text: str, warn_if_not_found: bool = True) -> str:
    match = get_correctly_formatted_tax_number_from_text(text)
    if match != None:
        return match

    if warn_if_not_found:
        logger.warn('Could not extract strict tax number from |%s|', text)

    return None

def get_tax_number_from_text(text: str, warn_if_not_found: bool = True) -> str:
    match = get_correctly_formatted_tax_number_from_text(text)
    if match != None:
        return match

    match = get_numbers_only_tax_number_from_text(text)
    if match != None:
        return match

    if warn_if_not_found:
        logger.warn('Could not extract tax number from |%s|', text)

    return None


def contains_digits(text: str) -> bool:
    return any(c.isdigit() for c in text)

def contains_date(text: str) -> bool:
    return None != get_date_from_text(text, False)

def contains_strict_tax_number(text: str) -> bool:
    return None != get_strict_tax_number_from_text(text, False)

def contains_tax_number(text: str) -> bool:
    return None != get_tax_number_from_text(text, False)


currencies = ['ft', 'huf', '€' 'eur', '$', 'usd']

def contains_currency(text: str) -> bool:
    return any([currency in text.lower() for currency in currencies])

def contains_decimal_point(text: str) -> bool:
    return None != decimal_point_matcher.search(clean_numberstring_of_ocr_errors(clean_whitespace(text)))

def to_ascii(string: str) -> str:
    """
    Converts accented characters to ascii characters. 
    """
    return unidecode.unidecode(string)