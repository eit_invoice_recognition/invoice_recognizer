from datetime import date
from typing import List
from enum import Enum, IntEnum
import json
import jsonpickle

class OCRMethod(Enum):
    AZURE = 1
    TESSERACT = 2
    OCRSPACE = 3

    def __str__(self):
        return self._name_

    def __repr__(self):
        return self.__str__()

class YDirection(IntEnum):
    UP = 1
    DOWN = -1

class FragmentSource(IntEnum):
    AZURE = 1
    TESSERACT = 2
    OCRSPACE = 3
    TEXT_PDF = 4

class InvoiceClassHandler(jsonpickle.handlers.BaseHandler):

    def flatten(self, obj, data):
        return obj._name_

    def restore(self, obj):
        return InvoiceClass[obj]

@InvoiceClassHandler.handles
class InvoiceClass(Enum):
    HANDWRITTEN = 1
    RECEIPT = 2
    MACHINE_PRINTED = 3
    TEXT_PDF = 4
    OTHER = 5

    def __str__(self):
        return self._name_

    def __repr__(self):
        return self.__str__()


class Fragment(object):
    __id = 1

    # x,y values refer to the upper left corner of the bounding box
    # The coordinate system's origin (0,0) is at the top left corner of the page with y pointing down
    def __init__(self,
        text: str, 
        x: float,
        y: float,
        width: float,
        height: float,
        page: int = None):
        self.page = page
        self.text = text
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.id = Fragment.__id
        Fragment.__id = Fragment.__id + 1

    def __str__(self):
        return ("Fragment(\npage=|%s|, id=|%s|, x=|%s|, y=|%s|, width=|%s|, height=|%s|, text=|%s|\n)\n\n" 
                    % (str(self.page), str(self.id), str(self.x), str(self.y), str(self.width), str(self.height), str(self.text)))

    def __eq__(self, other):
        if other == None:
            return False

        myId = self.id
        otherId = other.id
        if myId == otherId:
            return True
        else:
            return False

    __repr__ = __str__



class InvoiceDocument(object):

    def __init__(self,
        fragment_list: List[Fragment],
        y_direction: YDirection,
        fragment_source: FragmentSource,
        invoice_class: InvoiceClass = None,
        document_name: str = None):
        self.fragment_list = fragment_list
        self.y_direction = y_direction
        self.invoice_class = invoice_class
        self.fragment_source = fragment_source
        self.pages = max([fragment.page for fragment in fragment_list])
        self.document_name = document_name
        if fragment_list:
            self.min_x = min([fragment.x for fragment in fragment_list])
            self.max_x = max([fragment.x + fragment.width for fragment in fragment_list])
            self.min_y = min([fragment.y for fragment in fragment_list])
            self.max_y = max([fragment.y for fragment in fragment_list])
        else:
            self.min_x = None
            self.max_x = None
            self.min_y = None
            self.max_y = None

    def __str__(self):
        return json.dumps(self.__dict__, default=str, indent=4, separators=(',', ': '), sort_keys=False)

    def __repr__(self):
        return self.__str__()



class Address(object):

    def __init__(self,
        zip_code: int,
        city_name: str,
        street_name: str,
        street_type: str,
        building_number: str,
        block_name: str,
        building_name: str,
        floor: str,
        door_number):
        self.zip_code = zip_code
        self.city_name = city_name
        self.street_name = street_name
        self.street_type = street_type
        self.building_number = building_number
        self.block_name = block_name
        self.building_name = building_name
        self.floor =  floor
        self.door_number = door_number

class Party(object):

    def __init__(self, 
        tax_number: (str, Fragment), 
        name: str,
        short_name: str,
        address: Address = None):
        self.tax_number = tax_number
        self.name = name
        self.short_name = short_name
        self.address = address

    def __str__(self):
        return json.dumps(self.__dict__, default=str, indent=4, separators=(',', ': '), sort_keys=False)

    def __repr__(self):
        return self.__str__()

class InvoiceItem(object):

    def __init__(self,
        name: (str, Fragment) = None,
        unit: (str, Fragment) = None,
        quantity: (int, Fragment) = None,
        unit_net_price: (float, Fragment) = None,
        vat_percent: (float, Fragment) = None,
        net_price: (float, Fragment) = None,
        vat_amount: (float, Fragment) = None,
        price: (float, Fragment) = None):
        self.name = name
        self.unit = unit
        self.quantity = quantity
        self.unit_net_price = unit_net_price
        self.vat_percent = vat_percent
        self.net_price = net_price
        self.vat_amount = vat_amount
        self.price = price

class Invoice(object):
    

    def __init__(self, 
        invoice_number: (str, Fragment) = None,
        issuer: Party = None,
        customer: Party = None,
        date_of_invoice: (date, Fragment) = None,
        payment_due: (date, Fragment) = None,
        payment_date: (date, Fragment) = None,
        payment_method: (str, Fragment) = None,
        currency: (str, Fragment) = None,
        vat_percent: (float, Fragment) = None,
        net_price: (float, Fragment) = None,
        vat_amount: (float, Fragment) = None,
        price: (float, Fragment) = None):
        self.invoice_number = invoice_number
        self.issuer = issuer
        self.customer = customer
        self.date_of_invoice = date_of_invoice
        self.payment_due = payment_due
        self.payment_date = payment_date
        self.payment_method = payment_method
        self.currency =  currency
        self.vat_percent = vat_percent
        self.net_price = net_price
        self.vat_amount = vat_amount
        self.price = price


    def __str__(self):
        return json.dumps(self.__dict__, default=str, indent=4, separators=(',', ': '), sort_keys=False)

    def __repr__(self):
        return self.__str__()


class InvoiceRecognitionRequest(object):

    def __init__(self,
                file_name: str,
                file_bytes: bytes,
                ocr_method: OCRMethod = OCRMethod.AZURE,
                use_cache: bool = True,
                use_image_enhancement: bool = False,
                remote_opten: bool = False):
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.ocr_method = ocr_method
        self.use_cache = use_cache  
        self.use_image_enhancement = use_image_enhancement
        self.remote_opten = remote_opten

    def __str__(self):
        props = dict(self.__dict__)
        del props['file_bytes']
        return json.dumps(props, default=str, indent=4, separators=(',', ': '), sort_keys=False)

    def __repr__(self):
        return self.__str__()


class InvoiceRecognitionResponse(object):

    def __init__(self,
                response_code: int,
                response_time: float = None,
                error_message: str = None,
                invoice: Invoice = None,
                invoice_class: InvoiceClass = None):
            self.response_code = response_code
            self.response_time = response_time
            self.error_message = error_message
            self.invoice = invoice
            self.invoice_class = invoice_class

    def __str__(self):
        return json.dumps(self.__dict__, default=str, indent=4, separators=(',', ': '), sort_keys=False)

    def __repr__(self):
        return self.__str__()