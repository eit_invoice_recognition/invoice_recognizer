import os
import sys
import time
import json
import jsonpickle
from flask import Flask, flash, request, redirect, url_for
from flask_api import status
from werkzeug.utils import secure_filename
parent_path = os.path.abspath(os.path.join('..'))
if parent_path not in sys.path:
    sys.path.append(parent_path)
from backend.invoice_recognizer import recognize_invoice
from business_objects import InvoiceRecognitionRequest, InvoiceRecognitionResponse, OCRMethod
from logger_setup import getLogger
import config

"""
To run the server do the following:
1. Export export FLASK_APP=rest_server.py

unix: export FLASK_APP=rest_server.py
cmd: set FLASK_APP=rest_server.py
Powershell: $env:FLASK_APP = "rest_server.py"

2. Run the flask server

python -m flask run

"""

logger = getLogger(__name__)

jsonpickle.set_preferred_backend('json')
jsonpickle.set_encoder_options('json', ensure_ascii=False)

app = Flask(__name__)

ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg', 'bmp'])

def __get_file_extension(filename: str) -> str:
    return filename.rsplit('.', 1)[1].lower()

def __is_allowed_file(filename: str) -> bool:
    return '.' in filename and __get_file_extension(filename) in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    logger.debug('New request coming from: %s@%s', request.remote_user, request.remote_addr)
    if request.method == 'POST':
        start = time.time()
        # check if the post request has the file part
        if len(request.files) == 0:
            logger.warning('No file found in request: |%s|', request)
            return 'No file part', status.HTTP_400_BAD_REQUEST
        file = list(request.files.values())[0]

        if not __is_allowed_file(file.filename):
            extension = __get_file_extension(file.filename)
            logger.warning('Not allowed extension |%s|! Filename is |%s|', extension, file.filename)
            return 'File extension |{}| is not allowed! Allowed formats are: |{}|'.format(extension, ALLOWED_EXTENSIONS), status.HTTP_400_BAD_REQUEST

        invoice_request = InvoiceRecognitionRequest(file.filename, file.read())
        if 'ocr_method' in request.args: #TODO: Handle errors
            config.OCR_METHOD = request.args['ocr_method']
            invoice_request.ocr_method = OCRMethod[config.OCR_METHOD]
        if 'use_cache' in request.args:
            config.USE_CACHE = request.args['use_cache'].lower() == 'true'
            invoice_request.use_cache = config.USE_CACHE
        if 'preprocess_image' in request.args:
            config.PREPROCESS_IMAGE = request.args['preprocess_image'].lower() == 'true'
            invoice_request.use_image_enhancement = config.PREPROCESS_IMAGE
        if 'remote_opten' in request.args:
            config.REMOTE_OPTEN = request.args['remote_opten'].lower() == 'true'
            invoice_request.remote_opten = config.REMOTE_OPTEN

        response = recognize_invoice(invoice_request)
        end = time.time()
        response.response_time = end - start
        return json.dumps(json.loads(jsonpickle.encode(response, unpicklable=False, max_depth=5)), indent=4, sort_keys=True, ensure_ascii=False),    response.response_code

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <h2>Allowed extensions are: {}</h2>
    <table style="width:100%">
        <tr>
            <th>URL parameter</th>
            <th>Allowed values</th> 
            <th>Description</th>
            <th>Default value</th>
        </tr>
        <tr>
            <td>ocr_method</td>
            <td>{}</td> 
            <td>Decides which OCR engine to use</td>
            <td>AZURE</td>
        </tr>
        <tr>
            <td>use_cache</td>
            <td>[True, False]</td> 
            <td>Decides whether to cache the Azure OCR output and load OCR outputs from cache for this request</td>
            <td>True</td>
        </tr>
        <tr>
            <td>preprocess_image</td>
            <td>[True, False]</td> 
            <td>Decides whether to attempt at normalizing the image before sending them for OCR</td>
            <td>False</td>
        </tr>
        <tr>
            <td>remote_opten</td>
            <td>[True, False]</td> 
            <td>Use a remote, Azure-based OPTEN copy [True] or a local file [False]</td>
            <td>False</td>
        </tr>
    </table>
    <form method=post enctype=multipart/form-data>
        <input type=file name=file>
        <input type=submit value=Upload>
    </form>
    '''.format(ALLOWED_EXTENSIONS, str(['AZURE', 'TESSERACT', 'OCRSPACE']))

