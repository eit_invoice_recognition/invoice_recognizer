import pickle


class RenamingUnpickler(pickle.Unpickler):
    RENAME_MAP = {'parse_objects':'business_objects'}

    def find_class(self, module, name):
        if module in RenamingUnpickler.RENAME_MAP.keys():
            module = RenamingUnpickler.RENAME_MAP[module]
        return super().find_class(module, name)