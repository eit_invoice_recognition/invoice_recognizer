import io
import invoiceparser.text_pdf_parser as parser
from analyzer.fragment_annotation import FragmentAnnotationML, FragmentAnnotationRuleBased
import pickle

pdf_file_path = "G:\\invoices_pdf_text\\1001-HMDI_000000002-ebiz_handmade_document_in.pdf"


if __name__ == "__main__":
    fragments = parser.convert_pdf_to_fragments(pdf_file_path)
    invoice = FragmentAnnotationRuleBased().reconstruct_invoice_from_fragments(fragments)
    print(invoice)
    dictionary = {"1001-HMDI_000000002-ebiz_handmade_document_in.pdf": invoice}

    with io.open('ui/pickled_invoice', 'wb') as f:
        pickle.dump(dictionary, f)